NePCR = {}

local Parse = NeP.DSL.parse
local Fetch = NeP.Interface.fetchKey

function NePCR.dynEval(condition, spell)
	return Parse(condition, spell or '')
end

local function getUnitsAround(unit)
	local numberUnits = 0
	for i=1,#NeP.OM.unitEnemie do
		local Obj = NeP.OM.unitEnemie[i]
		if UnitAffectingCombat(unit) and NeP.Core.Distance(unit, Obj.key) <= 10 then
			numberUnits = numberUnits + 1
		end
	end
	return numberUnits
end

NeP.library.register('NePCR', {

	HolyNova = function(units)
		local minHeal = GetSpellBonusDamage(2) * 1.125
		local total = 0
		for i=1,#NeP.OM.unitFriend do
			local Obj = NeP.OM.unitFriend[i]
			if Obj.distance <= 12 then
				if max(0, Obj.maxHealth - Obj.actualHealth) > minHeal then
					total = total + 1
				end
			end
		end
		return total > units
	end,

	aDot = function(Spell, refreshAt, health, class)
		if not IsUsableSpell(Spell) then return false end
		if refreshAt == nil then refreshAt = 0 end
		if health == nil then health = 100 end
		if class == nil then class = 3 end
		local Spellname, Spellrank, Spellicon, SpellcastingTime, SpellminRange, SpellmaxRange, SpellID = GetSpellInfo(Spell)
		local SpellcastingTime = SpellcastingTime * 0.001
		for i=1,#NeP.OM.unitEnemie do
			local Obj = NeP.OM.unitEnemie[i]
			if (UnitAffectingCombat(Obj.key) or Obj.is == 'dummy') and Obj.class >= class then	
				if Obj.health < health
				and UnitCanAttack('player', Obj.key)
				and NeP.Core.Infront('player', Obj.key)
				and IsSpellInRange(Spellname, Obj.key)then
					local _,_,_,_,_,_,debuffDuration = UnitDebuff(Obj.key, Spellname, nil, 'PLAYER')
					if not debuffDuration or  - GetTime() < refreshAt then
						if debuffDuration == nil then debuffDuration = 0 end
						if NeP.DSL.Conditions['ttd'](Obj.key) > (debuffDuration + SpellcastingTime)
						or SpellcastingTime < 1 then
							NeP.Engine.ForceTarget = Obj.key
							return true
						end
					end
				end
			end
		end
		return false
	end,

	SAoEObject = function(Units, Distance)
		local UnitsAroundObject = {}
		for i=1,#NeP.OM.unitEnemie do
			local Obj = NeP.OM.unitEnemie[i]
			if UnitAffectingCombat(Obj.key) and Obj.distance <= Distance then
				UnitsAroundObject[#UnitsAroundObject+1] = {
					unitsAround = getUnitsAround(Obj.key),
					key = Obj.key
				}
			end
		end
		table.sort(UnitsAroundObject, function(a,b) return a.unitsAround < b.unitsAround end)
		if UnitsAroundObject[1].unitsAround > Units then
			-- set PEs parsed Target and return true
			NeP.Engine.ForceTarget = UnitsAroundObject[1].key
			return true
		end
		return false
	end

})

NeP.DSL.RegisterConditon("petinmelee", function(target)
   if FireHack then 
		return NeP.Core.Distance('pet', target) < (UnitCombatReach('pet') + UnitCombatReach(target) + 1.5)
	else
		-- Unlockers wich dont have UnitCombatReach like functions...
		return NeP.Core.Distance('pet', target) < 5
	end
end)

NeP.DSL.RegisterConditon("inMelee", function(target)
   return NeP.Core.UnitAttackRange('player', target, 'melee')
end)

NeP.DSL.RegisterConditon("inRanged", function(target)
   return NeP.Core.UnitAttackRange('player', target, 'ranged')
end)

NeP.DSL.RegisterConditon("power.regen", function(target)
  return select(2, GetPowerRegen(target))
end)

NeP.DSL.RegisterConditon("casttime", function(target, spell)
    local name, rank, icon, cast_time, min_range, max_range = GetSpellInfo(spell)
    return cast_time
end)

NeP.DSL.RegisterConditon("castwithin", function(target, spell)
	local SpellID = select(7, GetSpellInfo(spell))
	for k, v in pairs( NeP.ActionLog.log ) do
		local id = select(7, GetSpellInfo(v.description))
		if (id and id == SpellID and v.event == "Spell Cast Succeed") or tonumber( k ) == 20 then
			return tonumber( k )
		end
	end
	return 20
end)

NeP.DSL.RegisterConditon('twohand', function(target)
  return IsEquippedItemType("Two-Hand")
end)

NeP.DSL.RegisterConditon('onehand', function(target)
  return IsEquippedItemType("One-Hand")
end)