local config = {

	key = 'NeP_PriestHoly',
	title = 'Priest Holy',
	subtitle = 'Settings',
	color = 'FFFFFF',
	width = 180,
	height = 250,
	profiles = true,
	resize = false,
	config = {

		{ type = 'text' , text = 'Nothing here yet...'}

	}
}

NeP.Interface.buildGUI(config)

local init = function()
	NeP.Interface.CreateToggle('testToggle', 'This is just a testing toggle...', 'Interface\\ICONS\\Ability_repair.png')
	NeP.Interface.CreateSetting('Class Settings', function() NeP.Interface.ShowGUI('NeP_PriestHoly') end)
end

local Shared = {
	
	-- Buff
	{ 'Power Word: Fortitude', '!player.aura(stamina)', 'player' },

}

local inCombat = {

	-- Shared stuff
	{ Shared },

	{'Holy Word: Serenity', 'tank.health(50)', 'tank'},
	{'Holy Word: Serenity', 'lowest.health(100)', 'lowest'},
	{'Purify', 'player.dispellAll(Purify)', 'player'},
	{'Power Word: Shield', {'tank.health(100)', '!tank.debuff(Weakened Soul)'}, 'tank'},
	{'Renew', {'tank.health(100)', '!tank.buff(Renew)'}, 'tank'},
	{'Flash Heal', 'lowest.health(50)', 'lowest'},
	{'Power Word: Solace', nil, 'target'},
	{'Circle of Healing', 'player.AoEHeal(90, 3)', 'player'},
	{'Cascade', 'player.AoEHeal(90, 3)', 'player'},
	{'Heal', 'lowest.health(100)', 'lowest'},

}

local outCombat = {

	-- Shared stuff
	{ Shared },

}

NeP.Engine.registerRotation(257, 'Priest - Holy', inCombat, outCombat, init)