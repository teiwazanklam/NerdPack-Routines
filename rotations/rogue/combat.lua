local dynEval = NePCR.dynEval
local PeFetch = NeP.Interface.fetchKey

local config = {
	key = 'NePConfigRogueCombat',
	profiles = true,
	title = '|T'..NeP.Interface.Logo..':10:10|t'..NeP.Info.Nick..' Config',
	subtitle = 'Rogue Combat Settings',
	color = 'FFFFFF',
	width = 250,
	height = 500,
	config = {
		
		-- General
		{ type = 'header', text = 'General:', align = 'center' },
			-- ...Empty...
			{ type = 'text', text = 'Nothing here yet... :C', align = 'center' },
		-- Poisons
		{ type = "spacer" },{ type = 'rule' },
		{ type = 'header', text = 'Poisons:', align = 'center' },
			-- Letal Poison
			{ type = 'dropdown', text = 'Letal Posion', key = 'LetalPosion',
		      	list = {
			        {text = 'Wound Posion', key = 'Wound'},
			        {text = 'Deadly Posion', key = 'Deadly'},
			    },
		    	default = 'Deadly',
		    	desc = 'Select what Letal Posion to use.'
		    },
		    -- Non-Letal Poison
			{ type = 'dropdown',text = 'Non-Letal Posion',key = 'NoLetalPosion',
		      	list = {
			        {text = 'Crippling Poison', key = 'Crippling'},
			        {text = 'Leeching Posion', key = 'Leeching'},
			    },
		    	default = 'Crippling',
		    	desc = 'Select what Non-Letal Posion to use.'
		    },
		-- Survival
		{ type = 'spacer' },{ type = 'rule' },
		{ type = 'header', text = 'Survival:', align = 'center'},
			{type = 'spinner', text = 'Healthstone - HP', key = 'Healthstone', width = 50, default = 75},
	}
}

NeP.Interface.buildGUI(config)

local exeOnLoad = function()
	--NeP.Splash()
	NeP.Interface.CreateSetting('Class Settings', function() NeP.Interface.ShowGUI('NePConfigRogueCombat') end)
	NeP.Interface.CreateToggle(
		'MfD',
		'Use Marked for Death \nBest used for targets that will die under a minute.',
		'Interface\\Icons\\Ability_hunter_assassinate.png')
	NeP.Interface.CreateToggle(
		'dotEverything',
		'Click here to dot all (Rupture) the things while in Single Target.',
		'Interface\\Icons\\Ability_creature_cursed_05.png')
end

local Cooldowns = {
	{ 'Vanish' },
	{ 'Preparation', 'player.spell(Vanish).cooldown >= 10' },
	{ 'Adrenaline Rush', 'player.energy < 20' },
	{ 'Killing Spree', {
		'player.energy < 20',
		'player.spell(Adrenaline Rush).cooldown >= 10'
	}},
}

local Survival = {
	{ '#5512', (function() return dynEval('player.health <= ' .. PeFetch('NePConfigRogueCombat', 'Healthstone')) end) }, --Healthstone
	{'Recuperate',{
		'player.combopoints <= 3',
		'player.health < 35',
		'player.buff(Recuperate).duration <= 5'
	}},
	{'Tricks of the Trade', 'player.aggro > 60', 'tank'},
}

local AoE = {
	{ 'Blade Flurry' },
	{'Crimson Tempest', {
		'player.combopoints >= 5',
	}, 'target'},
}

local ST = {
	{{-- Auto Dotting
		{'Rupture', {
			'player.combopoints >= 5',
			'@NePCR.aDot(Rupture, 7)'
		}, 'target' },
	}, 'toggle.dotEverything' },
	{ 'Ambush' },
	{ 'Revealing Strike', '!target.debuff(Revealing Strike)', 'target' },
	{ 'Slice and Dice', {
		'!player.buff(Slice and Dice)',
		'player.combopoints < 4'
	}},
	{ 'Eviscerate', 'player.combopoints >= 5', 'target' },
	{ 'Sinister Strike' }
}

local outCombat = {
	{'8676', { -- Ambush after vanish
		'target.alive',
		'lastcast(1856)' -- Vanish
	}, 'target' },

	-- Letal Poisons
	{'2823', { -- Deadly Poison / Letal
		'!lastcast(2823)',
		'!player.buff(2823)',
		(function() return NeP.Interface.fetchKey('NePConfigRogueCombat', 'LetalPosion') == 'Deadly' end)
	}},
	{'8679', { -- Deadly Poison / Letal
		'!lastcast(8679)',
		'!player.buff(8679)',
		(function() return NeP.Interface.fetchKey('NePConfigRogueCombat', 'LetalPosion') == 'Wound' end)
	}},
	
	-- Non-Letal Poisons
	{'3408', { -- Crippling Poison / Non-Letal
		'!lastcast(3408)',
		'!player.buff(3408)',
		(function() return NeP.Interface.fetchKey('NePConfigRogueCombat', 'NoLetalPosion') == 'Crippling' end)
	}},
	{'108211', { -- Leeching Poison / Non-Letal
		'!lastcast(108211)',
		'!player.buff(108211)',
		(function() return NeP.Interface.fetchKey('NePConfigRogueCombat', 'NoLetalPosion') == 'Leeching' end)
	}},
}

NeP.Engine.registerRotation(260, 'Rogue - Combat', 
	{-- In-Combat
		{{ -- Dont Break Sealth && Melee Range
			{{-- Interrupts
				{ 'Kick' },
			}, 'target.interruptAt(40)' },
			{'Marked for Death', {
				'player.combopoints = 0',
				'toggle.MfD'
			}},
			{'Evasion', 'player.health < 30'},
			{Cooldowns, 'modifier.cooldowns' },
			{AoE, 'player.area(10).enemies >= 6' },
			{ST},
		}, {'!player.buff(Vanish)', 'target.range < 7'} },
	}, outCombat, exeOnLoad)